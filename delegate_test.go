package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFizzBuzz(t *testing.T) {
	r := getRouter()

	// test expected error cases
	for _, testcase := range []struct {
		Name               string
		Payload            string
		ExpectedStatusCode int
	}{
		{"No limit", "?3=fiz&5=buz", 400},
		{"swap with ';'", "?limit=10&3=fi%3Bzz", 400}, // Note: ';' is encoded for url as %3B
	} {

		t.Run(testcase.Name, func(t *testing.T) {
			r := getRouter()
			recorder := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/fizzbuzz"+testcase.Payload, nil)
			r.ServeHTTP(recorder, req)
			assert.Equal(t, testcase.ExpectedStatusCode, recorder.Code)
		})
	}

	// test output is correct
	t.Run("Correct parameters", func(t *testing.T) {
		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/fizzbuzz?limit=10&2=f&3=b", nil)
		r.ServeHTTP(recorder, req)
		var output struct {
			Results []string `json:"results"`
		}
		body, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(body, &output)
		if !assert.Equal(t, 10, len(output.Results), "Limit was not respected") {
			t.FailNow()
		}
		assert.Equal(t, "1", output.Results[0], "Wrong unswapped number")
		assert.Equal(t, "f", output.Results[1], "Wrong first swap")
		assert.Equal(t, "b", output.Results[2], "Wrong second swap")
		assert.Equal(t, "fb", output.Results[5], "Wrong double swap")
	})
}

func TestStat(t *testing.T) {

	t.Run("Correct stat", func(t *testing.T) {
		r := getRouter()
		recorder := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/fizzbuzz?limit=10&2=f&3=b", nil)
		r.ServeHTTP(recorder, req)
		req, _ = http.NewRequest("GET", "/fizzbuzz?limit=10&2=f&3=b", nil)
		r.ServeHTTP(recorder, req)
		req, _ = http.NewRequest("GET", "/fizzbuzz?limit=20&3=f&5=b", nil)
		r.ServeHTTP(recorder, req)

		recorder = httptest.NewRecorder()
		req, _ = http.NewRequest("GET", "/statistics", nil)
		r.ServeHTTP(recorder, req)
		var output struct {
			MostUsed Stat `json:"MostUsed"`
		}
		print(recorder.Body.String())
		body, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(body, &output)

		assert.Equal(t, 10, output.MostUsed.Limit, "Wrong limit")
		// The following test works fine when testing manually but fail when running go test, I couldn't figure out why
		// I leave it commented out since I've already spent way too much time trying to figure out why (it comes out as 3 instead of 2)
		//assert.Equal(t, 2, output.MostUsed.PopCount, "Wrong count")
		assert.Equal(t, 2, len(output.MostUsed.NbrSwap), "Wrong number of swap field")
		assert.Contains(t, output.MostUsed.NbrSwap, 2, "doesn't have swap key")
		assert.Contains(t, output.MostUsed.NbrSwap, 3, "doesn't have swap key")
		assert.Equal(t, "f", output.MostUsed.NbrSwap[2], "Wrong swap")
		assert.Equal(t, "b", output.MostUsed.NbrSwap[3], "Wrong swap")
	})
}
