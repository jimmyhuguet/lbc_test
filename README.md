# Interview exercise

Pour demarrer le server:

> $ go run .

Pour executer les tests:

> $ go test .

# API

## fizzbuzz: `GET /fizzbuzz`

> ? **limit**=\<int\> & \<**int**\>=\<string\>

**limit** specifie le nombre de valeur en retour

Chaque **int** en parametre represente un facteur a remplacer par la string fourni

L'example suivant:

> ? **limit**=10 & **2**=fizz & **3**=buzz

Renvoie la reponse suivante:
```json
{
  "results": [
    "1",
    "fizz",
    "buzz",
    "fizz",
    "5",
    "fizzbuzz",
    "7",
    "fizz",
    "buzz",
    "fizz"
  ]
}
```

## statistics: `GET /statistics`

Ne prend aucun parametre

Retourne la requete la plus frequente comme cela:

```json
{
  "requete": {
    "limit": 10,
    "swaps": {
      "2": "fizz",
      "3": "buzz"
    },
    "count": 1
  }
}
```