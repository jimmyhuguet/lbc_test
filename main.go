package main

import (
	"sort"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

type Stat struct {
	Limit    int            `json:"limit"`
	NbrSwap  map[int]string `json:"swaps"`
	PopCount int            `json:"count"`
}

// Normalize parameters for indexing
func (s Stat) toString() string {
	strList := []string{string(s.Limit)}
	keys := []int{}
	for k, _ := range s.NbrSwap {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for _, k := range keys {
		strList = append(strList, strconv.Itoa(k)+"="+s.NbrSwap[k])
	}
	return strings.Join(strList, ";")
}

var (
	allstat = map[string]Stat{}
	popStat string
	maxStat = 0
)

func getRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/fizzbuzz", FizzBuzz)
	r.GET("/statistics", Statictics)
	return r
}

func main() {
	r := getRouter()
	r.Run() // listen and serve on 0.0.0.0:8080
}
