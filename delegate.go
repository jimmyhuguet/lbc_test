package main

import (
	"sort"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func FizzBuzz(c *gin.Context) {
	// Extract parameter
	limitRequiredStr := "the GET parameter 'limit' is required and must be a number"
	stat := Stat{NbrSwap: map[int]string{}}
	limitList := c.Request.URL.Query()["limit"]
	if len(limitList) == 0 {
		c.JSON(400, gin.H{"error": limitRequiredStr})
		return
	}
	limit64, err := strconv.ParseInt(limitList[0], 10, 64)
	if err != nil {
		c.JSON(400, gin.H{"error": limitRequiredStr})
		return
	}
	stat.Limit = int(limit64)

	for key, val := range c.Request.URL.Query() {
		keyInt, err := strconv.ParseInt(key, 10, 64)
		if err == nil && keyInt != 0 {
			// Enforce a limitation on string swapping to avoid conflict in normalized stats
			if strings.Contains(val[0], ";") {
				c.JSON(400, gin.H{"error": "swapping strings cannot contains ';'"})
				return
			}
			stat.NbrSwap[int(keyInt)] = val[0]
		}
	}

	results := []string{}

	// Construct response
	keys := []int{}
	for k, _ := range stat.NbrSwap {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for i := 1; i <= stat.Limit; i++ {
		str := ""
		for _, k := range keys {
			if i%k == 0 {
				str += stat.NbrSwap[k]
			}
		}
		if str == "" {
			str = strconv.Itoa(i)
		}
		results = append(results, str)
	}

	// Update statistics
	idxString := stat.toString()
	if _, ok := allstat[idxString]; !ok {
		stat.PopCount = 1
		allstat[idxString] = stat
	} else {
		stat = allstat[idxString]
		stat.PopCount++
		allstat[idxString] = stat
	}
	if stat.PopCount > maxStat {
		maxStat = stat.PopCount
		popStat = idxString
	}

	c.JSON(200, gin.H{
		"results": results,
	})
}

func Statictics(c *gin.Context) {
	c.JSON(200, gin.H{
		"requete": allstat[popStat],
	})
}
